console.debug('production.js')

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js')
        .then(registration => {
            console.log('SW registered successfully')
        })
        .catch((error) => {
            console.error('Could not register service worker', error)
        })
}
