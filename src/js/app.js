// global JS goes here
console.debug('App.js')
window.$ = (selector) => document.querySelector(selector)
window.$$ = (selector) => document.querySelectorAll(selector)

window.addEventListener('online', onlineMode)
window.addEventListener('offline', offlineMode)

const toast = document.getElementById('toast')

function onlineMode() {
    document.head.querySelector('meta[name=theme-color]').content = '#4FC3F7'

    document.body.classList.remove('offline')
    toast.innerHTML = ''
    toast.classList.remove('show')
}

function offlineMode() {
    document.head.querySelector('meta[name=theme-color]').content = '#333333'

    document.body.classList.add('offline')

    toast.innerHTML = 'You are browsing in offline mode!'
    toast.classList.add('show')
}
