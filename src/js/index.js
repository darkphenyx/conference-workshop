/* eslint-disable no-undef */
console.debug('index.js')

// https://pwa-scratch.firebaseio.com/flights.json

fetch('https://pwa-scratch.firebaseio.com/flights.json')
.then(response => response.json())
.then(data => {
    let template = ''
    data.forEach(flight => {
        template += `<div class="flight">
<h3>${flight.city}, ${flight.country}</h3>
<p>$${flight.price} . ${flight.stops === 0 ? 'Nonstop' : flight.stops + ' stops'}</p>
</div>`
    })
    document.getElementById('booking').innerHTML = template
})
.catch((err) => console.error(err))
