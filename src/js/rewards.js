console.debug('Rewards JS')
// https://pwa-scratch.firebaseio.com/rewards.json

fetch('https://pwa-scratch.firebaseio.com/rewards.json')
    .then(response => response.json())
    .then(data => {
        $('#avatar').src = data.avatar
        $('#reward-number').innerHTML = data.reward_number
        $('#miles').innerHTML = data.miles
        $('#address').innerHTML = data.address
    })
    .catch(console.error)
