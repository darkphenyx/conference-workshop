const HtmlWebpackPlugin = require('html-webpack-plugin')
const VisualizerPlugin = require('webpack-visualizer-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')

const webpack = require('webpack')
const path = require('path')

var isProd = (process.env.NODE_ENV === 'production')

var devServer = {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    hot: true,
    inline: true,
    port: 3000,
    stats: 'errors-only',
    open: true
}
if (isProd) {
    devServer = {}
}

var plugins = []
plugins.push(new CopyWebpackPlugin([
    { from: 'src/assets', to: 'assets' }
]))
// comment out when needed (demo only)
// plugins.push(new CopyWebpackPlugin([
    // { from: 'src/service-worker.js' }
// ]));
plugins.push(new webpack.HashedModuleIdsPlugin())
plugins.push(new HtmlWebpackPlugin({
    hash: false,
    excludeChunks: ['rewards', 'more'],
    chunksSortMode: (c1, c2) => {
        let orders = ['app', 'index']
        let o1 = orders.indexOf(c1.names[0])
        let o2 = orders.indexOf(c2.names[0])
        return o1 - o2
    },
    template: './src/templates/index.hbs',
    filename: 'index.html',
    inject: true
}))
plugins.push(new HtmlWebpackPlugin({
    hash: false,
    excludeChunks: ['rewards', 'index'],
    chunksSortMode: (c1, c2) => {
        let orders = ['app', 'more']
        let o1 = orders.indexOf(c1.names[0])
        let o2 = orders.indexOf(c2.names[0])
        return o1 - o2
    },
    template: './src/templates/more.hbs',
    filename: 'more.html',
    inject: true
}))
plugins.push(new HtmlWebpackPlugin({
    hash: false,
    excludeChunks: ['index', 'more'],
    chunksSortMode: (c1, c2) => {
        let orders = ['app', 'rewards']
        let o1 = orders.indexOf(c1.names[0])
        let o2 = orders.indexOf(c2.names[0])
        return o1 - o2
    },
    filename: 'rewards.html',
    template: './src/templates/rewards.hbs',
    inject: true
}))
plugins.push(new ExtractTextPlugin({
    filename: 'css/[name]-[chunkhash].bundle.css',
    allChunks: true
}))
if (!isProd) {
    plugins.push(new webpack.HotModuleReplacementPlugin())
}
plugins.push(new webpack.NamedModulesPlugin())
plugins.push(new VisualizerPlugin({
    filename: '../bundleVisualizer.html'
}))
if (isProd) {
    plugins.push(new webpack.optimize.ModuleConcatenationPlugin())
    plugins.push(new UglifyJSPlugin())
}

const appScripts = [
    './src/js/app.js',
    './src/styles/app.scss'
]
if (isProd) {
    appScripts.push('./src/js/production.js')
}

if (isProd) {
    plugins.push(new SWPrecacheWebpackPlugin({
        staticFileGlobs: [
            'dist/*.html',
            'dist/js/*.js',
            'dist/css/*.css'
        ],
        stripPrefix: 'dist',
        minify: true,
        runtimeCaching: [{
            urlPattern: /pwa-scratch\.firebaseio\.com/,
            handler: 'fastest'
        }]
    }))
}

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: {
        app: appScripts,
        rewards: './src/js/rewards.js',
        index: './src/js/index.js',
        more: './src/js/more.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name]-[hash].bundle.js'
    },
    externals: [{
        'handlebars/runtime': {
            root: 'Handlebars',
            amd: 'handlebars.runtime',
            commonjs2: 'handlebars/runtime',
            commonjs: 'handlebars/runtime'
        }
    }],
    module: {
        rules: [
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader?runtime=handlebars/runtime'
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    require('autoprefixer')({
                                        browsers: ['last 2 versions']
                                    })
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            progressive: true,
                            optipng: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            mozjpeg: {
                                quality: 659
                            },
                            gifsicle: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            }
                        }
                    }
                ]
            }
        ]
    },
    devServer: devServer,
    plugins: plugins
}
