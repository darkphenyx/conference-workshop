#### Usage

- Run `npm run dev` for development with webpack dev-server and hot module replacement
- Run `npm run build` for optimized production build
- Run `npm run  serve` to build and serve the built assets to view things as a user


#### Folder Structure

##### Source Folder

```
└───src
    ├───<assets>
    │   ├───<images>      // all images
    │   └───<misc> 	  // manifest.json and robots.txt
    ├───<js>
    ├───<styles>
    │    ├───<anotherPage> 	// SASS for another page
    │    ├───<landingPage>      // SASS for landing page
    │    └────app.sass 	        // SASS for global    
    │
    ├───<templates> 
    │    └────portfolio-item.html // Portfolio Page
    │ 
    └───index.html // Landing Page 
    
 ```

 ##### Dist Folder
 
 ```
└───dist
	├───<css>	  // Compiled css
    ├───<images>   // Images
    ├───<js>       // JS Bundles
    │
    ├───<anotherPage> 
    │    └────another-page.html // Another Page
    │ 
    ├───index.html // Landing Page
    │
    ├───robots.txt
    └───manifest.json
    
 ```
